const loader = new THREE.FileLoader();
THREE.Cache.enabled = true;
var totalVideo = 0;
var totalLoad = 0;
var totalLoaded = 0;
var loading = 0;
var arrPreload = [];

// console.log("cargando...");
function loadFiles(file){
    loader.load(
        file,
        function ( data ){
            totalLoaded++;
            console.log(loading+"% "+totalLoaded+" de "+$("a-assets *").length);
            if(loading >= 99 || (totalLoaded+1) == $("a-assets *").length) {
                setTimeout(() => {
                    $(".loadGame").fadeOut(1000);
                }, 2000);
            }
        },
        function ( xhr ) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded' );
            totalLoad = parseInt((xhr.loaded / xhr.total * 100).toFixed());
            loading = (totalLoad/totalVideo)+((totalLoaded*(100/totalVideo)));
            console.log("Cargando: "+loading);
            // return new Promise(resolve => {
            //     return true;
            // });
            
        },
        function ( err ) {
            console.log("Error: "+err);
            // return new Promise(resolve => {
            //     return false;
            // });
        }
    );
}
async function run(){
    if($("a-assets *").length > 0){
        for (let index = 0; index < $("a-assets *").length; index++) {
            totalVideo++;
            // console.log("https://knimmersive.com/mina/bosqueVirtual/"+$("a-assets *")[index].getAttribute("src"));
            // await loadFiles("https://knimmersive.com/mina/bosqueVirtual/"+$("a-assets *")[index].getAttribute("src"));
            console.log("http://localhost:8888/bosqueVirtual/"+$("a-assets *")[index].getAttribute("src"));
            await loadFiles("http://localhost:8888/bosqueVirtual/"+$("a-assets *")[index].getAttribute("src"));
        }
        // $("a-assets *").each(async function(index){
        // });
    }
    else{
        setTimeout(() => {
            $(".loadGame").fadeOut(1000);
        }, 2000);
    }
}
run();