var data = [
    {
        id: 0,
        animal:"Pez burrito rayado",
        retro: "",
        pregunta:"",
        respuestas:[
            {
                id: 0,
                respuesta: "Suele evitar las aguas transparentes.",
            },
            {
                id: 1,
                respuesta: "Tiene una longitud máxima de 18 cm.",
            },
            {
                id: 2,
                respuesta: "Se encuentra en peligro de extinción."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 1,
        animal:"Langosta espinosa",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Aunque normalmente mide 20 cm, puede llegar a medir hasta 60.",
            },
            {
                id: 1,
                respuesta: "Se alimenta principalmente de cangrejos ermitaños, caracoles marinos, almejas y mejillones.",
            },
            {
                id: 2,
                respuesta: "Es nocturna y su mayor depredador es el hombre."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 2,
        animal:"Delfín nariz de botella",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Suele medir 3 metros de largo.",
            },
            {
                id: 1,
                respuesta: "Utiliza la ecolocalización para ubicarse sin necesidad de la vista.",
            },
            {
                id: 2,
                respuesta: "Tiene un promedio de vida de 50 a 60 años."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 3,
        animal:"Pez mero goliat atlántico",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Pesa más de 300 kg y mide en promedio 175 cm de longitud.",
            },
            {
                id: 1,
                respuesta: "Se alimenta principalmente de langostas espinosas, aunque también come tortugas, pulpos y peces.",
            },
            {
                id: 2,
                respuesta: "Habita en promedio a 46 m de profundidad."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 4,
        animal:"Tortuga verde",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Puede llegar a pesar 300 kg y medir 1.5 metros.",
            },
            {
                id: 1,
                respuesta: "Inicia siendo omnívora pero una vez que es adulta se vuelve herbívora.",
            },
            {
                id: 2,
                respuesta: "Se encuentra en el océano atlántico y en el pacífico."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 5,
        animal:"Tortuga carey",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Pesa de 45 a 75 kg y mide entre 70 y 90 cm.",
            },
            {
                id: 1,
                respuesta: "Su alimento preferido son las esponjas marinas.",
            },
            {
                id: 2,
                respuesta: "Tiende a anidar de forma solitaria."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 6,
        animal:"Tortuga canal",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Puede llegar a pesar más de 500 kg y mide entre 1.3 y 1.8 metros. ",
            },
            {
                id: 1,
                respuesta: "Se alimenta casi exclusivamente de medusas.",
            },
            {
                id: 2,
                respuesta: "Es la única tortuga marina sin caparazón duro."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    },
    {
        id: 7,
        animal:"Pez cerdo",
        retro: "",
        pregunta:"",
        respuestas: [
            {
                id: 0,
                respuesta: "Llega a medir hasta 40 cm.",
            },
            {
                id: 1,
                respuesta: "Se alimenta de estrellas de mar, anémonas y esponjas.",
            },
            {
                id: 2,
                respuesta: "Alcanza hasta los 90 m de profundidad."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0,
        calif:0
    }
];