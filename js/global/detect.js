
var detectIn;
AFRAME.registerComponent('cursor-listener', {
    init: function () {
        /* Entra al área de un animal */
        this.el.addEventListener('mouseenter', function (evt) {
            var camera = document.querySelector('a-entity#camera');
            var target = this;
            var inCircle = {
                radiusInner: 0.10,
                radiusOuter: 0.13
            }
            detectIn = anime({
                targets: inCircle,
                easing: 'linear',
                radiusInner: 0.01,
                radiusOuter: 0.04,
                duration: 1000,
                loop:false,
                autoplay: false,
                update: function() {
                    camera.setAttribute('geometry', inCircle );
                },
                complete: function(){
                    camera.setAttribute('geometry',{
                        radiusInner: 0.10,
                        radiusOuter: 0.13
                    });
                    camera.setAttribute('material','color','#ffb236');
                    clicked(target);
                }
            });
            detectIn.restart();
        });
        /* Sale del área de un animal  */
        this.el.addEventListener('mouseleave', function (evt) {
            $(".camera").animate({width: "70px", height: "70px"},300);
            camera.setAttribute('material','color','#ffb236');
            detectIn.pause();
            camera.setAttribute('geometry',{
                radiusInner: 0.10,
                radiusOuter: 0.13
            });
        });
        /* Click arriba de un animal  */
        this.el.addEventListener('mouseup', function (evt) {
        });
        /* Click abajo de un animal  */
        this.el.addEventListener('mousedown', function (evt) {
        });
        this.el.addEventListener('click', function (evt) {
        });
    }
});
function clicked(target){
    if(!target.classList.contains("contestado")){
        if($('body').hasClass("find")){
            if(validar(target.id)){
                // correctas();
                // target.setAttribute('material', 'color', 'green');
                audioCorrecto.play();
                // $(".validator, .validar .sombra").addClass("correct");
                validatorCircle.setAttribute('src', '#respuestaBien' );
                aCircleValidator.play();
                anime_validation_MC.play();
                correctas++;
                target.classList.add("contestado");
                // console.log(`url(../assets/img/oceano/fauna/${target.getAttribute("animal")}.png`);
                $(".validar .animalValidar").css(`background-image`,`url(assets/img/oceano/fauna/${target.getAttribute("animal")}.png`);
                // $(`.puntuacion .punto.[data-id="${correctas}"]`).addClass('resuelto');
                $(`.puntuacion .punto.active`).addClass('resuelto');
                target.setAttribute('material', 'color', '#808080');
            }
            else{
                // console.log(`url(../assets/img/oceano/fauna/${target.getAttribute("animal")}.png`);
                $(".validar .animalValidar").css(`background-image`,`url(assets/img/oceano/fauna/${target.getAttribute("animal")}.png`);
                // $(".validator, .validar .sombra").addClass("error");
                validatorCircle.setAttribute('src', '#respuestaMal' );
                aCircleValidator.play();
                anime_validation_MC.play();
                // target.setAttribute('material', 'color', 'red');
                audioInCorrecto.play();
            }
        }
        else{
            if($('body').hasClass("MultipleChoice") && !target.classList.contains("contestado")){
                if(target.classList.contains("opcionesVR")){
                    responderMC(target.getAttribute("data-idAnimal"));
                }else{
                    target.classList.add("contestado");
                    target.setAttribute('material', 'color', '#808080');
                    getMultiplechoice(parseInt(target.getAttribute('id'))-1);
                    // console.log(target.getAttribute('rotation'));
                }
            }
        }
    }
}
AFRAME.registerComponent('boton-vr', {
    init: function () {
        /* Entra al área de un animal */
        this.el.addEventListener('mouseenter', function (evt) {
            var camera = document.querySelector('a-entity#camera');
            var target = this;
            var inCircle = {
                radiusInner: 0.10,
                radiusOuter: 0.13
            }
            detectIn = anime({
                targets: inCircle,
                easing: 'linear',
                radiusInner: 0.01,
                radiusOuter: 0.04,
                duration: 1000,
                loop:false,
                autoplay: false,
                update: function() {
                    camera.setAttribute('geometry', inCircle );
                },
                complete: function(){
                    camera.setAttribute('geometry',{
                        radiusInner: 0.10,
                        radiusOuter: 0.13
                    });
                    camera.setAttribute('material','color','#ffb236');
                    clickedButton(target);
                }
            });
            detectIn.restart();
        });
        /* Sale del área de un animal  */
        this.el.addEventListener('mouseleave', function (evt) {
            detectIn.pause();
        });
        /* click arriba de un animal  */
        this.el.addEventListener('mouseup', function (evt) {
        });
        /* click abajo de un animal  */
        this.el.addEventListener('mousedown', function (evt) {
        });
        this.el.addEventListener('click', function (evt) {
        });
    }
});
function clickedButton(target){
    console.log(target.id);
    switch (target.id) {
        case "btn_fb":
            window.location.replace('https://www.facebook.com/sharer/sharer.php?u=https://bosquevirtual.cobrepanama.com','_blank');
            break;
        case "btn_tw":
            window.location.replace('https://twitter.com/intent/tweet?text=Visita%20y%20juega%20en%20el%20Bosque%20Virtual%20de%20Cobre%20Panama&url=https://bosquevirtual.cobrepanama.com&hashtags=cobrepanama','_blank');
            break;
        case "btn_wa":
            window.location.replace('https://api.whatsapp.com/send?text=Visita%20y%20juega%20en%20el%20Bosque%20Virtual%20de%20Cobre%20Panama','_blank');
            break;
        case "btn_replay":
            scorebtnUno.play();
            window.location.replace('./acuatica.html');
            break;
        case "btn_inicio":
            scorebtnDos.play();
            window.location.replace('./');
            break;
        case "btn_audio":
            playAudio($("a-sky").attr("data-land"),data[indexAnimal].animal, false);
            break;
        case "btn_audioSig":
            playAudio($("a-sky").attr("data-land"),data[indexAnimal].animal, true);
            break;
        case "btn_ayuda":
            ayuda();
            break;
        case "Instr_btn_jugar":
            scaleVRInstr.reverse();
            cerrarInstruccion();
            break;
        case "Instr_btn_menu":
            window.location.replace('./');
            break;
        default:
            break;
    }
}