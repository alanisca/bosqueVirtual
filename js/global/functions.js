var audioCorrecto = new Howl({src: [`./assets/audio/correcto.mp3`]});
var audioInCorrecto = new Howl({src: [`./assets/audio/incorrecto.mp3`]});

var scorebtnUno = new Howl({src: [`./assets/audio/incorrecto.mp3`]});
var scorebtnDos = new Howl({src: [`./assets/audio/incorrecto.mp3`]});
var scoreGenerico = new Howl({src: [`./assets/audio/score_generico.mp3`]});
var scoreCompleto = new Howl({src: [`./assets/audio/score_completo.mp3`]});

var audioClick = new Howl({src: [`./assets/audio/menu.mp3`]});
var audioAyuda = new Howl({src: [`./assets/audio/ayuda.mp3`]});
var audiobocina = new Howl({src: [`./assets/audio/bocina.mp3`]});
var audiotimepo = new Howl({src: [`./assets/audio/tiempo.mp3`],loop:true});
var audioInstruction;
var arrAudioFaltante = [];

var vrMode = false;
if($("body").attr("data-land") == "acuatico") var soundsaudioAmbiente = new Howl({src:[`./assets/audio/oceano/ambiente.mp3`],loop:true,autoplay: true});
if($("body").attr("data-land") == "terrestre") var soundsaudioAmbiente = new Howl({src:[`./assets/audio/bosque/ambiente.mp3`],loop:true,autoplay: true});
if($("body").attr("data-land") == "aereo") var soundsaudioAmbiente = new Howl({src:[`./assets/audio/aerea/ambiente.mp3`],loop:true,autoplay: true});

var soundInstrucciones = new Howl({src:[`./assets/audio/${$("a-sky").attr("data-land")}/Instrucciones.mp3`]});
// if($("body").attr("data-land") == "acuatico" || $("body").attr("data-land") == "aereo") 
//     var soundInstrucciones = new Howl({src:[`./assets/audio/InstruccionesTiempo.mp3`]});
// else
    // var soundInstrucciones = new Howl({src:[`./assets/audio/oceano/InstruccionesOpMult.mp3`]});





var gameStarted = false;
var correctas = 0;
var contestadas = 0;
/* Variable de animal actual */
var indexAnimal = 0;
/* Variable de audio */
var indexAudio = 0;
$( document ).ready(function() {
    
});
if(!$("body").hasClass("find")) {
    $(".icon.audio, .icon.sigAudio, .timer").css("display","none");
}
function initMiniGameFind(){
    $('#blockGameSphere').attr("scale","0 0 0");
    shuffleArray(data);
    getScore();
    playAudio($("a-sky").attr("data-land"),indexAnimal, true);
}
function initMiniGameMC(){
    getScore();
}
function getScore(){
    if(localStorage.getItem("acuatico")) {
        $(".puntuacionContain .acuatico .puntos").text(localStorage.getItem("acuatico"));
        $(".vr_score_acuatico").attr("text","value :"+localStorage.getItem("acuatico"));
    }
    if(localStorage.getItem("terrestre")) {
        $(".puntuacionContain .terrestre .puntos").text(localStorage.getItem("terrestre"));
        $(".vr_score_terrestre").attr("text","value :"+localStorage.getItem("terrestre"));
    }
    if(localStorage.getItem("aereo")) {
        $(".puntuacionContain .aereo .puntos").text(localStorage.getItem("aereo"));
        $(".vr_score_aereo").attr("text","value :"+localStorage.getItem("aereo"));
    }
}
function playAudio(land, iAnimal, nextAudio = false){
    if(nextAudio) indexAudio++;
    if(indexAudio > 3 ) {
        data.push(data.splice(indexAnimal,1)[0])
        // arrAudioFaltante.push(indexAnimal);
        indexAudio = 1;
    }
    anim_playGame.pause();
    anim_time_min.pause();
    anim_time_seg.pause();
    $('.blockGame').css('display','block');
    $('#blockGameSphere').attr("scale","1 1 1");
    $(`.pointsContain .puntuacion .punto`).removeClass("active");
    $(`.pointsContain .puntuacion .punto[data-id="${data[indexAnimal].id}"]`).addClass("active");
    audioInstruction = new Howl({src: [`./assets/audio/${land}/0${(data[indexAnimal].id+1)}/audio0${indexAudio}.mp3`]});
    audioInstruction.play();

    audioInstruction.on('end', function() {
        setTimeout(() => {
            $('.blockGame').css('display','none');
            $('#blockGameSphere').attr("scale","0 0 0");
        }, 1100);
        if($("body").hasClass("find")){
            anim_playGame.play();
            anim_time_min.play();
            anim_time_seg.play();
        }
        $(".animate__heartBeat").removeClass("animate__heartBeat");
    }, false);
}
function playAudioMC(land, iAnimal, type = "pregunta"){
    $('.blockGame').css('display','block');
    $('#blockGameSphere').attr("scale","1 1 1");
    $(`.pointsContain .puntuacion .punto`).removeClass("active");
    $(`.pointsContain .puntuacion .punto[data-id="${(data[indexAnimal].id - 1)}"]`).addClass("active");

    var audioInstruction = new Howl({src: [`./assets/audio/${land}/0${(data[iAnimal].id)}/${type}.mp3`]});
    // var audioInstruction = new Audio(`./assets/audio/${land}/0${(data[iAnimal].id)}/${type}.mp3`);
    audioInstruction.play();
    if(audioInstruction.paused) {
        // audioInstruction.play();
        // .catch((e) => {
        //     if(type == "retro") $(".multiple-choice").fadeOut(1000);
        //     setTimeout(() => {
        //         $('.blockGame').css('display','none');
        //         $('#blockGameSphere').attr("scale","0 0 0");
        //         $(".respuesta").removeClass("incorrecto correcto animate__pulse");
        //     }, 1100);
        //     if($(".punto").length <= $(".punto.contestado").length){
        //         endMiniGame( $(".punto.resuelto").length);
        //     }
        //     console.error(e);
        // });
    }
    audioInstruction.on('end', function() {
        if(type == "retro") {
            $(".multiple-choice").fadeOut( 1000 );
            $("#MC").attr("visible",false);
            $("#MC").attr("position","0 -100 0");
            if($(".punto").length <= $(".punto.contestado").length){
                endMiniGame( $(".punto.resuelto").length);
            }
        }
        setTimeout(() => {
            $(".animate__heartBeat").removeClass("animate__heartBeat");
            $('.blockGame').css('display','none');
            $('#blockGameSphere').attr("scale","0 0 0");
            $(".respuesta").removeClass("incorrecto correcto animate__pulse");
        }, 1100);
        // anim_playGame.play();
    }, false);
    
}
function endMiniGame(pts = 0){
    if($(".punto").length <= $(".punto.resuelto").length)
        scoreCompleto.play();
    else 
        scoreGenerico.play();
    $('#blockGameSphere').attr("scale","1 1 1");
    //SI existe info almacenada y es menor a la lograda se coloca en los punto
    if( localStorage.getItem($("body").attr("data-land")) < (pts * 10) ){
        $(`.puntuacionContain .${$("body").attr("data-land")} .puntos`).text(pts * 10);
        $("#scoreTot").attr("text","value :"+pts * 10);
        $(".vr_score_"+$("body").attr("data-land")).attr("text","value :"+pts * 10);
    }
    //Si lo jugó previamente
    if(localStorage.getItem($("body").attr("data-land"))){
        if(localStorage.getItem($("body").attr("data-land")) < (pts * 10)) 
            localStorage.setItem($("body").attr("data-land"), pts * 10);
    }
    else{
        localStorage.setItem($("body").attr("data-land"), pts * 10);
    }
    var totalGame = 0;
    $(".puntuacionFinal table .puntos").each(function(){
        if($(this).text() != "-") totalGame += parseInt($(this).text());
    });
    $(".puntuacionContain .puntuacionFinal .scores .score .totalScore").html(totalGame);
    $("#scoreTot").attr("text","value :"+totalGame);

    if(vrMode){
        finalScoreVR.play();
    }
    else{
        $(".puntuacionContain").css("display","flex");
    }
}
function sendMessage(title, message, action){
    $('.instrucciones .title').html(title);
    $('.instrucciones .text').html(message);
    $('.instrucciones .play').html(action);
}

function validar(animalId){
    var validado = false;
    // console.log(animalId, data[indexAnimal].id, indexAnimal);
    if(animalId == data[indexAnimal].id){
        $("#animalName").attr({
            "text":"value:"+data[indexAnimal].animal
        });
        animalName.play();
        indexAnimal++;
        indexAudio = 0;
        validado = true;
        anim_playGame.pause();
        anim_time_min.pause();
        anim_time_seg.pause();
        setTimeout(() => {
            $("#animalName").attr("scale","0 0 1");
            // getMultiplechoice(indexAnimal);
            if($(".punto").length > $(".punto.resuelto").length){
                playAudio($("a-sky").attr("data-land"),data[indexAnimal].animal, true);
            }
            else{
                endMiniGame( $(".punto.resuelto").length);
            }
        }, 2000);
    }
    return validado;
}

function shuffleArray(array) {
    let currentIndex = array.length,  randomIndex;
    // While there remain elements to shuffle...
    while (currentIndex != 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    return array;
}
$(document).ready(function(){
    // console.log( "ready!" );
    // $(".loadGame").fadeOut(1000);
    $("a-sky .animal").each(function(index){
        $(".puntuacion").append(`<div class="punto" data-id="${(index)}"><span></span></div>`)
    });
    // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    //     console.log('Esto es un dispositivo móvil');
    // }
    // else{
    //     console.log('Esto es una PC');
    //     $("a-scene").attr("vr-mode-ui","enabled: false");
    // }
    $(window).bind("orientationchange", function(event){		
        if (event.orientation == 'landscape') {
            $('.orientationContain').css("display","none");
            if(gameStarted) {
                if($("body").hasClass("find")){
                    anim_playGame.play();
                    anim_time_min.play();
                    anim_time_seg.play();
                }
            }
        }
        else{
            $('.orientationContain').css("display","block");
            anim_playGame.pause();
            anim_time_min.pause();
            anim_time_seg.pause();
        }
    });
    $( window ).orientationchange();
});
/* Cerrar instrucciones e iniciar juego */
$(".messageContainer .instrucciones .play").on("click",cerrarInstruccion);
function cerrarInstruccion(){
    soundInstrucciones.pause();
    $('#blockGameSphere').attr("scale","0 0 0");
    if(vrMode){
        if(scaleVRInstr.direction == 'normal') scaleVRInstr.reverse();
        scaleVRInstr.play();
    }
    else{
        $(".ayuda").removeClass("animate__pulse");
        if(anim_instrucctionOff.reversed) anim_instrucctionOff.reverse();
        anim_instrucctionOff.play();
        anim_instrucctionOff.finished.then(function(){
            $('.messageContainer').css('display','none');
            if(!gameStarted && $("body").hasClass("find")) {
                initMiniGameFind();
                gameStarted = true;
            }
            else{
                if($("body").hasClass("MultipleChoice")){
                    initMiniGameMC();
                    gameStarted = true;
                }
            }
        });
        setTimeout(() => {
            $(this).html("Regresar");
        }, 2500);
    }

}
$(".ayuda").on("click",function(){
    ayuda();
});
$(".playInstrucciones").on("click",function(){
    soundInstrucciones.stop();
    soundInstrucciones.play();
});
function ayuda(){
    $('#blockGameSphere').attr("scale","1 1 1");
    audioAyuda.play();
    anim_playGame.pause();
    anim_time_min.pause();
    anim_time_seg.pause();
    if(vrMode){
        if(scaleVRInstr.direction == 'reverse') scaleVRInstr.reverse();
        scaleVRInstr.play();
    }
    else{
        $(".ayuda").addClass("animate__pulse");
        $('.messageContainer').css('display','block');
        if(!anim_instrucctionOff.reversed) anim_instrucctionOff.reverse();
        anim_instrucctionOff.play();
    }

}
$(".audio").on("click",function(){
    audiobocina.play();
    $(this).addClass("animate__heartBeat");
    if($("body").hasClass("MultipleChoice")){
        playAudioMC($("a-sky").attr("data-land"),indexAnimal, "pregunta");//Reproduce la retro
    }else{
        playAudio($("a-sky").attr("data-land"),indexAnimal, false);
    }
});
$(".sigAudio").on("click",function(){
    audiobocina.play();
    $(this).addClass("animate__heartBeat");
    playAudio($("a-sky").attr("data-land"),indexAnimal, true);
});

function getMultiplechoice(index){
    // alert(data[index].animal);
    indexAnimal = index;
    playAudioMC($("a-sky").attr("data-land"), index,"pregunta")
    shuffleArray(data[index].respuestas);
    if(vrMode){
        $("#txt1").attr("text","value:"+data[index].respuestas[0].respuestaN+";").attr("data-idAnimal",data[index].respuestas[0].id);
        $("#txt2").attr("text","value:"+data[index].respuestas[1].respuestaN+";").attr("data-idAnimal",data[index].respuestas[1].id);
        $("#txt3").attr("text","value:"+data[index].respuestas[2].respuestaN+";").attr("data-idAnimal",data[index].respuestas[2].id);
        $("#animal").attr("src",'#animal0'+(index+1));
        $("#animal").attr("geometry",`width:${data[index].width}; height:${data[index].height};`)

        $("#MC").attr("visible",true);
        $("#MC").attr("position","0 0 0");
        $("#MC").attr("rotation",0+" "+($("#fcamera").attr("rotation").y*-1)+" "+$("#fcamera").attr("rotation").z);
    }
    else{
        $(".multiple-choice .nombre").html(`<i>${data[index].animal}</i>`);
        $(".icon.audio").addClass("audioActivoMC");
        console.log(data[index].animalIMG,"assets/img/bosque/fauna/"+data[index].animalIMG+".png");
        $(".multiple-choice .animalIMG").css({"background-image":"url(assets/img/bosque/fauna/"+data[index].animalIMG+".png)"});
        $(".multiple-choice .opciones .respuesta[data-id='0']").html(data[index].respuestas[0].respuesta).attr("data-idAnimal",data[index].respuestas[0].id);
        $(".multiple-choice .opciones .respuesta[data-id='1']").html(data[index].respuestas[1].respuesta).attr("data-idAnimal",data[index].respuestas[1].id);
        $(".multiple-choice .opciones .respuesta[data-id='2']").html(data[index].respuestas[2].respuesta).attr("data-idAnimal",data[index].respuestas[2].id);
        $(".multiple-choice").fadeIn();
    }
}
// $(".multiple-choice").on("click",function(){$(this).fadeOut();})
/* Validar Opción multiple */
$(".multiple-choice .opciones .respuesta").on("click",function(){
    responderMC($(this).attr("data-idAnimal"), $(this));
});
function responderMC(idAnimal, $respuesta ){
    var correctMC = parseInt(idAnimal);
    contestadas++;
    if(data[indexAnimal].correctas.indexOf(correctMC) >= 0) {
        correctas++;
        data[indexAnimal].calif = 10;
        // $(`.puntuacion .punto[data-id="${contestadas}"]`).addClass('resuelto');
        $(`.puntuacion .punto.active`).addClass('resuelto contestado');
        audioCorrecto.play();
        if(vrMode){
            
        }
        else{
            $respuesta.addClass("correcto animate__pulse");
        }
    }
    else {
        audioInCorrecto.play();
        $(`.puntuacion .punto.active`).addClass('noResuelto contestado');
        if(vrMode){
            
        }
        else{
            $respuesta.addClass("incorrecto animate__pulse");
        }
    }
    if($("body").hasClass("find")){
        anim_playGame.play();
        anim_time_min.play();
        anim_time_seg.play();
    }
    playAudioMC( $("a-sky").attr("data-land"),indexAnimal, "retro" );//Reproduce la retro
    $(".icon.audio").removeClass("audioActivoMC");
}
document.querySelector('a-scene').addEventListener('enter-vr', function () {
    vrMode = true;
    $('#btn_audio').attr("scale","-1 1 1");
    $('#btn_audioSig').attr("scale","-1 1 1");
    $('#btn_ayuda').attr("scale","-1 1 1");
});
document.querySelector('a-scene').addEventListener('exit-vr', function () {
    vrMode = false;
    $('#btn_audio').attr("scale","0 0 0");
    $('#btn_audioSig').attr("scale","0 0 0");
    $('#btn_ayuda').attr("scale","0 0 0");
});