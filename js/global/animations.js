/* Animacion */
var updateV = 0;
var reverse = true;
// var totalAnimals = $('a-sky').attr('total-animals');
var totalAnimations = $('a-sky').attr('total-animations');
var anim_animal05 = anime({
    targets: '',
    easing: 'linear',
    translateX:100, 
    duration: 100,
    loop:true,
    // autoplay: false,
    begin: function(){
    },
    loopBegin: function(){
        $('.animal').each(function(index){
            $(this).attr('src','#animal0'+(index+1)+'_0'+updateV);
        });
        if(reverse) updateV++;
        else updateV--;
        if(updateV > totalAnimations ){
            reverse = false;
            updateV = totalAnimations-1;
        }
        if(updateV < 0 ){
            reverse = true;
            updateV = 1;
        }
    }
});

/* Barra de tiempo */
var anim_playGame = anime({
    targets:'.timer .loading',
    height:['99%','0%'],
    duration:60000,
    easing:'linear',
    autoplay: false,
    update: function(anim){
        if(anim.progress > 80){
            $(".timer .time .loading").css("background-color","#FF0308");
            if(!audiotimepo.playing()) audiotimepo.play();
        }
    },
    begin: function(){
        
    },
    complete: function(anim){
        audiotimepo.pause();
        endMiniGame( $(".punto.resuelto").length);
    }
});

/* Animacion de instrucciones  */
var anim_instrucctionOff = anime.timeline({
    duration: 500,
    easing:'linear',
    autoplay:false,
    begin: function(){
        anim_playGame.pause();
        $('.messageContainer').css('display','block');
    },
    complete: function(){
        
    }
});
anim_instrucctionOff.add({
    targets: '.messageContainer .instrucciones',
    scale:[1,1.2,0],
    easing:'linear'
});
anim_instrucctionOff.add({
    targets: '.messageContainer',
    opacity: 0
});
/* Animacion multiple choice */
var anim_circle_MChoice = anime({
    targets:'.multiple-choice .cirle.big',
    rotate: '360deg',
    duration: 10000,
    easing: 'easeInQuad',
    autoplay: true,
    loop: true
});
var anim_circle_MChoice2 = anime({
    targets:'.multiple-choice .cirle.small',
    rotate: '-360deg',
    duration: 15000,
    easing: 'easeInOutCirc',
    autoplay: true,
    loop: true
});
/* Reloj */
var anim_time_min = anime({
    targets:'.timer .reloj .borderReloj .minutes',
    rotate: '360deg',
    duration: 5000,
    easing: 'linear',
    autoplay: false,
    loop: true
});
var anim_time_seg = anime({
    targets:'.timer .reloj .borderReloj .seconds',
    rotate: '360deg',
    duration: 30000,
    easing: 'linear',
    autoplay: false,
    loop: true
});
/* Validar */
var anime_validation_MC = anime({
    targets: '.validar .sombra',
    duration: 1500,
    easing: 'linear',
    begin: function(){
        $(".validar").css("display","flex");
    },
    complete: function(){
        $(".validar").css("display","none");
    },
    scale: [0,1],
    opacity: [0,1,.5,0],
    autoplay: false
});
var validatorCircle = document.querySelector('#validator')
var circleValidator = {
    height: 0.001,
    width: 0.001
}
var circleOpacity = {
    opacity:1,
}
var aCircleValidator = anime({
    targets: [circleValidator,circleOpacity],
    easing: 'linear',
    height: 4,
    width: 4,
    opacity: 0,
    duration: 1000,
    loop:false,
    autoplay: false,
    update: function() {
        validatorCircle.setAttribute('geometry', circleValidator );
        validatorCircle.setAttribute('material', circleOpacity );
    },
    complete: function(){
        validatorCircle.setAttribute('geometry',{
            height: 0.001,
            width: 0.001
        });
    }
});
//animacion final
var scaleVRobj = {scaleVR:0};
var finalScoreVR = anime({
    targets: scaleVRobj,
    scaleVR: 1,
    easing: 'linear',
    duration:500,
    autoplay:false,
    update: function() {
        $("#fondocierre_vr_obj").attr("scale",scaleVRobj.scaleVR+" "+scaleVRobj.scaleVR);
    },
    begin: function(){
        $("#cierreScore_vr").attr("scale","-1 1 1");
        $("#cierreScore_vr").attr("rotation",$("#fcamera").attr("rotation").x+" "+($("#fcamera").attr("rotation").y*-1)+" "+$("#fcamera").attr("rotation").z);
    }
});
//Instrucciones VR
var scaleVRInstr = anime({
    targets: scaleVRobj,
    scaleVR: 1,
    easing: 'linear',
    duration:500,
    autoplay:false,
    update: function() {
        $("#Instr_Buscar_obj").attr("scale",scaleVRobj.scaleVR+" "+scaleVRobj.scaleVR);
    },
    begin: function(){
        $("#instruccion_vr").attr("scale","-1 1 1");
        $("#instruccion_vr").attr("rotation",$("#fcamera").attr("rotation").x+" "+($("#fcamera").attr("rotation").y*-1)+" "+$("#fcamera").attr("rotation").z);
    },
    complete: function(){
        if(scaleVRInstr.direction == 'reverse') $("#instruccion_vr").attr("scale","0 0 0");
    }
});
// mouse
var inCircle = {
    radiusInner: 0.10,
    radiusOuter: 0.13
}
var detectIn = anime({
    targets: inCircle,
    easing: 'linear',
    radiusInner: 0.01,
    radiusOuter: 0.04,
    duration: 1000,
    autoplay: false,
    update: function() {
        camera.setAttribute('geometry', inCircle );
    },
    complete: function(){
        camera.setAttribute('geometry',{
            radiusInner: 0.10,
            radiusOuter: 0.13
        });
        camera.setAttribute('material','color','#ffb236');
    }
});
/* nombre de animal find */
var animalName = anime({
    targets: scaleVRobj,
    scaleVR: 1,
    visible:true,
    duration: 1000,
    update: function() {
        $("#animalName").attr("scale",scaleVRobj.scaleVR+" "+scaleVRobj.scaleVR);
    },
    complete: function(){
    }
});