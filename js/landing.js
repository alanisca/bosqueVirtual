var vOrientation = '';
var audiomenuGenerico = new Audio(`./assets/audio/menuGenerico.mp3`);

var audioMenuAcuatico = new Audio(`./assets/audio/menuAcuatico.mp3`);
var audioMenuAereo = new Audio(`./assets/audio/menuAereo.mp3`);
var audioMenuTerrestre = new Audio(`./assets/audio/menuTerrestre.mp3`);

$(".barra.aereas").on("click",function(){audioMenuAereo.play();})
$(".barra.terrestre").on("click",function(){audioMenuTerrestre.play();})
$(".barra.oceano").on("click",function(){audioMenuAcuatico.play();})

$(document).ready(function(){
    console.log("landscape");
    $(".barra").hover(function(event){
        // if($(this).children('.subBarra').parent('.barra').hasClass('terrestre')) audioMenuTerrestre.play();
        // if($(this).children('.subBarra').parent('.barra').hasClass('aereas')) audioMenuAereo.play();
        // if($(this).children('.subBarra').parent('.barra').hasClass('oceano')) audioMenuAcuatico.play();
        if (vOrientation == 'landscape') {
            $('.subBarra').css("right",0);
            if($(this).children('.subBarra').parent('.barra').hasClass('terrestre') )
                $(this).children(".subBarra").animate({"top": "0%"});
            else
                if($(this).hasClass("oceano")) $(this).children(".subBarra").animate({"top": "-112%"});
                else $(this).children(".subBarra").animate({"top": "-100%"});
        }
        else{
            $('.subBarra').css("top",0);
            if($(this).children('.subBarra').parent('.barra').hasClass('terrestre') )
            if($(this).hasClass("oceano")) $(this).children(".subBarra").animate({"right": "-112%"});
            else $(this).children(".subBarra").animate({"right": "-100%"});
        else
            $(this).children(".subBarra").animate({"right": "0%"});
        }
    }, function(){
        if (vOrientation == 'landscape') {
            if($(this).children('.subBarra').parent('.barra').hasClass('terrestre') )
                if($(this).hasClass("oceano")) $(this).children(".subBarra").animate({"top": "-112%"});
                else $(this).children(".subBarra").animate({"top": "-100%"});
            else
                $(this).children(".subBarra").animate({"top": "0%"});
        }
        else{
            if($(this).children('.subBarra').parent('.barra').hasClass('terrestre') )
            $(this).children(".subBarra").animate({"right": "0%"});
        else
            if($(this).hasClass("oceano")) $(this).children(".subBarra").animate({"right": "-112%"});
            else $(this).children(".subBarra").animate({"right": "-100%"});
        }
    });
    $(".contenedor").css("display","block");
});
$(window).bind("orientationchange", function(event){		
    if (event.orientation == 'landscape') {
        vOrientation = 'landscape';
    }
    else{
        vOrientation = 'portrain';
    }
});
$( window ).orientationchange();