var data = [
    {   
        id:1,
        animal:"CONEJO <strong>PINTADO</strong>",
        animalIMG:"Agouti",
        pregunta:"¿Qué medidas toma Panamá respecto a la caza del conejo pintado?",
        retro:"¿Sabías que las principales causas que atentan contra esta especie son la caza y la destrucción de su hábitat?",
        respuestas:[
            {
                id: 0,
                respuesta: "Su aprovechamiento es permitido </br>pero regulado por autoridades </br>en materia ambiental.",
                respuestaN: "Su aprovechamiento es permitido \n pero regulado por autoridades \n en materia ambiental."
            },
            {
                id: 1,
                respuesta: "Su caza está estrictamente prohibida.",
                respuestaN: "Su caza está \n estrictamente prohibida."
            },
            {
                id: 2,
                respuesta: "Su caza es permitida y no ha sido regulada en Panamá.",
                respuestaN: "Su caza es permitida y\nno ha sido regulada en Panamá."
            },
            
        ],
        correctas:[0],
        width:"1.2",
        height:"0.7",
        intentosMC:0,
        intentosQ:0
    },
    {   
        id:2,
        animal:"<strong>TAPIR</strong>",
        animalIMG:"BairdsTapir",
        pregunta:"¿Cuántas crías de tapir nacen en cada camada?",
        retro:"¿Sabías que tan solo de 10 a 15 minutos después de nacida, esta cría de tapir ya puede caminar junto a su madre?",
        respuestas:[
            {
                id: 0,
                respuesta: "Solo una cría de </br>aproximadamente 10 kg.",
                respuestaN: "Solo una cría de \n aproximadamente 10 kg."
            },
            {
                id: 1,
                respuesta: "Aproximadamente 3 crías con un </br>peso de 5 kg cada una.",
                respuestaN: "Aproximadamente 3 crías con un \n peso de 5 kg cada una."
            },
            {
                id: 2,
                respuesta: "Suelen ser 3 crías, cada una </br>con peso de 3 kg.",
                respuestaN: "Suelen ser 3 crías, cada una \n con peso de 3 kg."
            },
            
        ],
        correctas:[0],
        width:"1.6",
        height:"1.1",
        intentosMC:0,
        intentosQ:0
    },
    {   
        id:3,
        animal:"<strong>SAÍNO</strong>",
        animalIMG:"CollaredPecari",
        pregunta:"¿De qué se alimenta el saíno?",
        retro:"¿Sabías que en América esta especie se encuentra desde el sur de Estados Unidos hasta el norte de Argentina?",
        respuestas:[
            {
                id: 0,
                respuesta: "Se alimenta de frutos, tubérculos, <br>pastos, invertebrados.",
                respuestaN: "Se alimenta de frutos, \n tubérculos, pastos, invertebrados."
            },
            {
                id: 1,
                respuesta: "Su dieta consiste principalmente </br>de conejos.",
                respuestaN: "Su dieta consiste principalmente \n de conejos."
            },
            {
                id: 2,
                respuesta: "Se alimenta exclusivamente </br>de pasto y hojas.",
                respuestaN: "Se alimenta exclusivamente \n de pasto y hojas."
            },
            
        ],
        correctas:[0],
        width:"1.2",
        height:"1.0",
        intentosMC:0,
        intentosQ:0
    },
    {   
        id:4,
        animal:"OSO <strong>CABALLO</strong>",
        animalIMG:"Giantanteater",
        pregunta:"¿Cuánto pesa el oso caballo?",
        retro:"¿Sabías que estos osos no tienen dientes? Así es, usando solo sus lenguas ingieren alrededor de 35, 000 hormigas y termitas diarias.",
        respuestas:[
            {
                id: 0,
                respuesta: "Un macho adulto puede </br>pesar incluso 60 kg.",
                respuestaN: "Un macho adulto puede \n pesar incluso 60 kg."
            },
            {
                id: 1,
                respuesta: "Un macho adulto puede </br>pesar hasta 40 kg.",
                respuestaN: "Un macho adulto puede \n pesar hasta 40 kg."
            },
            {
                id: 2,
                respuesta: "Un macho adulto puede pesar </br>un máximo de 100 kg.",
                respuestaN: "Un macho adulto puede pesar \n un máximo de 100 kg."
            },
            
        ],
        correctas:[0],
        width:"1.6",
        height:"1.1",
        intentosMC:0,
        intentosQ:0
    },
    {   
        id:5,
        animal:"<strong>JAGUAR</strong>",
        animalIMG:"Jaguar",
        pregunta:"¿Qué tan grande es el jaguar?",
        retro:"¿Sabías que en Cobre Panamá nos aliamos con la Fundación Yaguará Panamá para implementar programas de conservación de los felinos de la zona y sus hábitats, en 3 áreas protegidas?",
        respuestas:[
            {
                id: 0,
                respuesta: "Es el felino más grande de América </br>con aproximadamente 0.75 m de </br>altura y 2 m de largo.",
                respuestaN: "Es el felino más grande de América \n con aproximadamente 0.75 m de  \n altura y 2 m de largo."
            },
            {
                id: 1,
                respuesta: "Es el segundo felino más grande del mundo con aproximadamente 1 m de altura y 2.5 m de largo.",
                respuestaN: "Es el segundo felino más grande \n del mundo con aproximadamente \n 1 m de altura y 2.5 m de largo."
            },
            {
                id: 2,
                respuesta: "Es el tercer felino más grande de América con 0.5 m de altura y </br>1.5 m de largo.",
                respuestaN: "Es el tercer felino más grande \n de América con 0.5 m de altura y \n 1.5 m de largo."
            },
            
        ],
        correctas:[0],
        width:"2.1",
        height:"1.1",
        intentosMC:0,
        intentosQ:0
    },
    {
        id:6,
        animal:"SERPIENTE DE <strong>CORAL</strong>",
        animalIMG:"PanamenianCoralSnake",
        retro: "Estas serpientes, a pesar de tener un veneno letal, no son agresivas.",
        pregunta:"¿De qué se alimentan?",
        respuestas:[
            {
                id: 0,
                respuesta: "Otras serpientes<br>más pequeñas.",
                respuestaN: "Otras serpientes\nmás pequeñas."
            },
            {
                id: 1,
                respuesta:  "Humanos.",
                respuestaN:  "Humanos."
            },
            {
                id: 2,
                respuesta: "Una variedad de insectos.",
                respuestaN: "Una variedad de insectos."
            },
           
        ],
        correctas:[0],
        width:"1.3",
        height:"1.1",
        intentosMC:0,
        intentosQ:0
    },
    {
        id:7,
        animal:"RANA <strong>ARLEQUÍN</strong>",
        animalIMG:"VariableHarlequinFrog",
        retro: "Este exótico sapo está en peligro de extinción a causa de la destrucción de su hábitat.",
        pregunta:"¿Qué caracteriza al sapo Arlequín?",
        respuestas:[
            {
                id: 0,
                respuesta: "Es poco cautelosa y mala nadadora.",
                respuestaN: "Es poco cautelosa y \n mala nadadora."
            },
            {
                id: 1,
                respuesta: "Es extremadamente venenosa.",
                respuestaN: "Es extremadamente venenosa."
            },
            {
                id: 2,
                respuesta: "Puede encontrarse en <br>toda Centroamérica.",
                respuestaN: "Puede encontrarse en \n toda Centroamérica."
            },
        ],
        correctas:[0],
        width:"1.0",
        height:"0.7",
        intentosMC:0,
        intentosQ:0
    },
    {
        id:8,
        animal:"ZAMIA <strong>SKINNERI</strong>",
        animalIMG:"ZamiaSkinneri",
        retro: "Esta especie con tallo corto y hojas grandes es una de las 17 especies de zamias que existen en Panamá y es de las plantas más antiguas.",
        pregunta:"¿Qué tan antigua es esta especie?",
        respuestas:[
            {
                id: 0,
                respuesta: "Es una especie muy <br>antigua, algo así como <br>“los dinosaurios de las plantas”.",
                respuestaN: ""
            },
            {
                id: 1,
                respuesta: "Es la zamia más nueva que <br>existe en Panamá.",
                respuestaN: ""
            },
            {
                id: 2,
                respuesta: "Tiene alrededor de 50 años en Panamá.",
                respuestaN: ""
            },
        ],
        correctas:[0],
        width:"0.9",
        height:"1.4",
        intentosMC:0,
        intentosQ:0
    },
    {
        id:9,
        animal:"CALATHEA <strong>GORDONII</strong>",
        animalIMG:"CalatheaGordonii",
        retro: "Sabías que es una de las 35 especies del género calathea que existen en Panamá y fue reconocida como especie endémica de Panamá en 2014.",
        pregunta:"¿Cuánto mide aproximadamente este tipo de planta?",
        respuestas:[
            {
                id: 0,
                respuesta: "30 cm de alto",
                respuestaN: ""
            },
            {
                id: 1,
                respuesta: "1 m de alto",
                respuestaN: ""
            },
            {
                id: 2,
                respuesta: "Medio metro de alto",
                respuestaN: ""
            },
        ],
        correctas:[0],
        width:"1.0",
        height:"1.0",
        intentosMC:0,
        intentosQ:0
    },
    {
        id:10,
        animal:"TRICHODRYMONIA <strong>PELTATIFOLIA</strong>",
        animalIMG:"TrichodrimoniaPeltatifolia",
        retro: "Esta planta es una de las 8 especies del género Trichodrymonia que existen en Panamá y se caracteriza por tener hojas redondas y peltadas.",
        pregunta:"¿Qué caracteriza a esta planta?",
        respuestas:[
            {
                id: 0,
                respuesta: "Es epífita, es decir, usa otras <br>plantas u objetos como soporte <br>sin ser parásito de ellos.",
                respuestaN: ""
            },
            {
                id: 1,
                respuesta: "Es reconocida por ser una <br>especie parásita.",
                respuestaN: ""
            },
            {
                id: 2,
                respuesta: "Pueden llegar a medir hasta <br>3m de alto.",
                respuestaN: ""
            },
        ],
        correctas:[0],
        width:"1.0",
        height:"1.0",
        intentosMC:0,
        intentosQ:0
    }
];