var data = [
    {
        id: 0,
        animal:"Águila Crestada",
        respuestas:[
            {
                id: 0,
                respuesta: "Ave de presa de tamaño mediano, que tiene una apariencia robusta.",
            },
            {
                id: 1,
                respuesta: "Es muy sigilosa y la emboscada es su método preferido de cacería.",
            },
            {
                id: 2,
                respuesta: "Su cresta occipital de plumas se levanta cuando está alerta o irritada."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
    {
        id: 1,
        animal:"Caracara",
        respuestas:[
            {
                id: 0,
                respuesta: "Habitualmente se alimenta de carroña.",
            },
            {
                id: 1,
                respuesta: "Suele poner 2 o 3 huevos que son incubados por ambos sexos",
            },
            {
                id: 2,
                respuesta: "Es un ave oportunista que puede robar el alimento de otras aves."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
    {
        id: 2,
        animal:"Gran guacamaya verde",
        respuestas:[
            {
                id: 0,
                respuesta: "Tiene una cola puntiaguda y muy larga.",
            },
            {
                id: 1,
                respuesta: "Normalmente se encuentra en parejas o en pequeños grupos de hasta 4-8 aves.",
            },
            {
                id: 2,
                respuesta: "Emite llamadas roncas y extremadamente fuertes, que se oyen a gran distancia."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
    {
        id: 3,
        animal:"Loro Real",
        respuestas:[
            {
                id: 0,
                respuesta: "Es un ave monógama con pocas crías.",
            },
            {
                id: 1,
                respuesta: "Es el loro más grande en México.",
            },
            {
                id: 2,
                respuesta: "Se trafica ilegalmente por su habilidad de imitar sonidos."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
    {
        id: 4,
        animal:"Philodendron llanense",
        respuestas:[
            {
                id: 0,
                respuesta: "Esta planta crece adherida al tronco de los árboles.",
            },
            {
                id: 1,
                respuesta: "Es una de las 100 especies del género Philodendron que existen en Panamá.",
            },
            {
                id: 2,
                respuesta: "Esta planta crece a varios metros sobre el nivel del suelo en bosques maduros."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
    {
        id: 5,
        animal:"Tucán",
        respuestas:[
            {
                id: 0,
                respuesta: "Su pico es mayormente hueco y le permite conservar o eliminar calor.",
            },
            {
                id: 1,
                respuesta: "Mide entre 18 y 63 cm de largo y se caracteriza por su gran y colorido pico.",
            },
            {
                id: 2,
                respuesta: "No es tan buen volador, por lo que se mueve mayormente saltando entre los árboles."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
    {
        id: 6,
        animal:"Águila harpía",
        respuestas:[
            {
                id: 0,
                respuesta: "Es el ave nacional de Panamá.",
            },
            {
                id: 1,
                respuesta: "Las dimensiones promedio de la hembra mayor son 100 cm de largo y 200 cm de envergadura, que es la distancia entre los extremos de las alas.",
            },
            {
                id: 2,
                respuesta: "Es una especie superpredadora. Sus presas favoritas son los monos, perezosos o coatíes."
            }
        ],
        correctas:[0],
        intentosMC:0,
        intentosQ:0
    },
];